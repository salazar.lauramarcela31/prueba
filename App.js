import React from 'react';
import LoginScreen from './src/auth/screens/login/LoginScreen';
import ListUserScreen from './src/users/list-users/ListUserScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import UserScreen from './src/users/user/UserScreen';
const Stack = createStackNavigator();//inicializa la navegación

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={LoginScreen} options={{headerShown: false}} />
        <Stack.Screen name="ListUsers" component={ListUserScreen} options={{headerShown: false}} />
        <Stack.Screen name="User" component={UserScreen} options={{headerShown: false}} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
