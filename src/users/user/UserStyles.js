import {Dimensions, StyleSheet} from 'react-native';

const UserStyles = StyleSheet.create({
    //Propiedades generales de la vista de usuario
    container: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center"
    },
    //Propiedades generales de las imagenes de usuario
    imageContainer:{
        flexDirection: "row",
        height: Dimensions.get('screen').height * 0.3,
        width: Dimensions.get('screen').width * 0.7,
        margin: 10,
    },
    //Propiedades generales de la info de usuario
    userInfoContainer: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center"
    },
    //Propiedades generales de los botones
    buttonsContainer: {
        flexDirection: "row"
    },
    //Propiedades generales de los estilos en los botones
    buttoStyle: {
        backgroundColor: "blue",
        margin: 10,
        padding: 10,
        width: Dimensions.get('screen').width * 0.2,
        alignItems: "center",
        borderRadius: 10
    }
});

export default UserStyles;