import {Dimensions, StyleSheet} from 'react-native';

const CardComponentStyles = StyleSheet.create({
    //Propiedades generales
    container: {
        height: Dimensions.get('screen').height * 0.2,
        width: Dimensions.get('screen').width * 0.9,
        flexDirection: "row",
        margin: 20
    },
    //Propiedades De la imagen
    containerImage: {
        flex: 1
    },
    //Propiedades del contenedor de los datos del usuario
    containerUserData: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    }
});

export default CardComponentStyles;