import { StyleSheet, Dimensions } from 'react-native';

const ButtonStyles = StyleSheet.create({ //Estilos para los botones
    //Estilo de boton pequeño
    small: {
        width: Dimensions.get("screen").width * 0.5,
        marginTop: 20,
        padding: 17,
        borderRadius: 50,
        alignItems: "center"
    },
    //Estilo de boton mediano
    medium: {
        width: Dimensions.get("screen").width * 0.7,
        marginTop: 20,
        padding: 17,
        borderRadius: 50,
        alignItems: "center"
    },
    //Estilo de boton grande
    large: {
        width: Dimensions.get("screen").width * 0.85,
        marginTop: 20,
        padding: 17,
        borderRadius: 50,
        alignItems: "center"
    }
});

export default ButtonStyles;