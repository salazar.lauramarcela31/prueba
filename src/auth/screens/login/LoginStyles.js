import { StyleSheet, Dimensions } from 'react-native';

const LoginStyles = StyleSheet.create({
    //Propiedades Generales
    container: {
        flex: 1,
    },
    //Propiedades del contenedor del logo
    logoContainer: {
        backgroundColor: 'orange',
        height: Dimensions.get("screen").height * 0.4,
        borderBottomLeftRadius: 100,
        justifyContent: "center",
        alignItems: "center"
    },
    //Propiedades del logo
    logo: {
        width: 120,
        height: 120
    },
    //Propiedades del titulo
    title: {
        position: "absolute",
        right: 30,
        bottom: 50,
        fontSize: 22,
        color: "white"
    },
    //Propiedades del contenedor del formulario login
    inputDataContainer: {
        height: Dimensions.get("screen").height * 0.60,
        alignItems: "center",
        paddingTop: 30,
        paddingBottom: 30
    },
    //Propiedades de los inputs del formulario login
    inputData: {
        backgroundColor: "white",
        width: Dimensions.get("screen").width * 0.85,
        borderRadius: 50,
        paddingLeft: 20,
        marginTop: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },
    //Texto del boton ingresar
    textButton: {
        color: "white",
        fontWeight: '400'
    },
    //Boton ingresar
    buttonGradient:{
        flex: 1
    }
});

export default LoginStyles;